These files comprise the workload of a graduate-level course in data mining. They are written in Python and interface with Spark via PySpark.

Most assignments mandated that only RDDs were to be used, not Spark Dataframes.

## Homework 1
Simple RDD transformations and transactions in mining a portion of Yelp data.

## Homework 2
The discovery of frequent itemsets/"baskets" in grocery store data by means of A-Priori and Park-Chen-Yu algorithms inside the framework of the Savasere-Omiecinski-Navathe algorithm.

## Homework 3
The discovery of similar businesses by locality sensitive hashing for Jaccard and cosine similiarities. Also, the building of basic model-based collaborative filtering recommendation systems on the same Yelp data.

## Homework 4
Community detection on graphs by Label Propagation Algorithm (Task 1, implemented by GraphFrames library) and Girvan-Newman Algorithm (Task 2).

## Homework 5
Data stream techniques. Bloom filtering on Yelp data to confirm whether a state value has already been seen. Flajolet-Martin to estimate the number of unique cities seen in the Yelp data. Fixed-sized sampling on Twitter data.