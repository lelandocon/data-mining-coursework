'''
This is task 1 from the third homework in INF 553: Data Mining
It entails the usage of Jaccard and cosine LSH to find similar items
'''

from pyspark import SparkContext, SparkConf
import sys
import time
from math import sqrt
import logging
import itertools
import random
from functools import partial
from operator import itemgetter

def standardMinHash(numberToHash, a, b, numBuckets):
    return (a*numberToHash+b)%numBuckets

def computeJaccardSimilarity(object0:set, object1:set) -> float:
    '''
    Performa simple Jaccard similarity calculation
    :param object0: a set of elements for one object
    :param object1: the same, but for another object
    :return: a float representing the Jaccard similarity
    '''
    return len(object0.intersection(object1))/len(object0.union(object1))

def computeCosineSimilarity(vector0, vector1):
    '''
    Performs simple cosine similarity calculation
    :param vector0: a tuple of the form (UserIndexToRatingDict, UsersSet)
    :param vector1: a tuple of the same form
    :return: a float representing the cosine similarity
    '''
    commonUsers=vector0[1].intersection(vector1[1])
    if len(commonUsers)==0:
        cosineSimilarity=0.0
    else:
        dotProductSum=0.0
        vector0Squares=0.0
        vector1Squares=0.0
        for commonUser in commonUsers:
            rating0ComUser=float(vector0[0].get(commonUser))
            rating1ComUser=float(vector1[0].get(commonUser))
            dotProductSum+=rating0ComUser*rating1ComUser
            vector0Squares+=rating0ComUser**2
            vector1Squares+=rating1ComUser**2
        cosineSimilarity=dotProductSum/sqrt(vector0Squares)/sqrt(vector1Squares)
    return cosineSimilarity


def createJaccardLSHFamily(numMembers, numBuckets, seed):
    logging.basicConfig()
    minHashFamily=list()
    '''
    numSteps = math.sqrt(numMembers)
    a=numBuckets/2+seed
    b=a/numSteps
    for coef in range(a, a*numSteps, a):
        for additive in range(b, b*numSteps, b):
            def anotherMember(numberToHash):
                return (coef*numberToHash+additive)%numBuckets
            minHashFamily.append(anotherMember)
    assert len(minHashFamily)==numMembers
    '''
    random.seed(seed)
    for i in range(numMembers):
        a = random.randint(1, numBuckets-1)
        b = random.randint(0, 10**9) #TODO: fix this arbitrary constant
        '''
        def anotherMember(numberToHash):
            return (a*numberToHash+b)%numBuckets
        '''
        #logging.info(f"a is {a} and b is {b}")
        newMinHashFunc=partial(standardMinHash, a=a, b=b, numBuckets=numBuckets)
        minHashFamily.append(newMinHashFunc)
    for i in range(len(minHashFamily)-1):
        assert id(minHashFamily[i])!=id(minHashFamily[i+1])
    return minHashFamily

def createMinHashSignature(vector, minHashFamily):
    '''
    Creates a min hash signature from a vector by applying
    each hash function in a family of functions to the vector
    :param vector: a tuple of form (item ID, nestedList), where nestedList has entries with the form (row ID, rating)
    :param minHashFamily: a list of hash functions that transmute the row number
    :return: a tuple of form (item id, signature), where signature is a list of the same length as the minHashFamily
    '''
    logging.basicConfig()
    signature=list()
    itemID=vector[0]
    rows=vector[1]
    # PLAN: iterate over the min hash family
    for minHashFunc in minHashFamily:
        #print(f"The id of the minhash function is {id(minHashFunc)}")
        lowestHash=None
        minHashValue=None
        for row in rows:
            rowID=row[0]
            hashedRowNum=minHashFunc(rowID)
            #print(f"hashedRowNum is {hashedRowNum} and lowestHash is {lowestHash}")
            if lowestHash==None or hashedRowNum<lowestHash:
                lowestHash=hashedRowNum
                minHashValue=rowID
        signature.append(minHashValue)
    # PLAN: iterate over the non-zero rows in the characteristic matrix
    assert len(signature)==len(minHashFamily), "The signature does not have the same length as the minhash family"
    return (itemID, signature)

def createCosineLSHFamily(numMembers, numDimensions, seed):
    # PLAN: create random vectors
    hyperplaneFamily=list()
    random.seed(seed)
    for i in range(numMembers):
        randomVector=random.getrandbits(numDimensions)
        hyperplaneFamily.append(randomVector)
    return hyperplaneFamily

def createCosineLSHSignature(vector, hyperplaneFamily):
    '''
    Creates a cosine LSH signature (which will have binary values representing which side of the hyperplane that
    the item vector is on) with the given hyperplaneFamily
    :param vector: a tuple of form (item ID, nestedList), where nestedList has entries with the form (row ID, rating)
    :param hyperplaneFamily: a list of integers, where each integer represents a bit vector of a certain length
    :return: a tuple of form (item id, signature), where signature is a list of the same length as the hyperplaneFamily
    '''
    NEGATIVE_ONE=0
    POSITIVE_ONE=1
    NONPOSITIVE_SUM=0
    POSITIVE_SUM=1
    signature = list()
    itemID = vector[0]
    rows = vector[1]
    # PLAN : iterate ove the members of the hyperplane family
    for hyperplaneVector in hyperplaneFamily:
        dotProductSum=0
        # PLAN : perform a dot product with the hyperplane normal vector and the item vector
        for row in rows:
            rowID=row[0]
            rating=float(row[1])
            hyperplaneComponent=(hyperplaneVector>>rowID)&1  #checking the appropriate value in the hyperplane vector
            if hyperplaneComponent==POSITIVE_ONE:
                dotProductSum+=rating
            else:
                assert hyperplaneComponent==NEGATIVE_ONE, "Hyperplane component is neither 0 or 1, it is %d"%hyperplaneComponent
                dotProductSum-=rating
        # PLAN : add a number to the signature based on the sign of the dot product
        if dotProductSum>=0:
           signature.append(POSITIVE_SUM)
        else:
            signature.append(NONPOSITIVE_SUM)
    return (itemID, signature)

def banding(itemIDAndSig, numBands, numRows):
    '''
    USED IN CONJUNCTION WITH FLATMAP
    Takes a signature of a given item and yields many bands, each one of which hashes to a value that may be the
    same as the exact same band from a different item
    :param itemIDAndSig: a tuple of the form (itemID, signature)
    :param numBands: an integer denoting the number of bands
    :param numRows: an
    :return:
    '''
    # PLAN: split up the signature into bands
    itemID, signature=itemIDAndSig
    bands=[tuple(signature[i:i+numRows]) for i in range(0, len(signature), numRows)]
    assert len(bands)==numBands, "The length of bands %d is not the same as the number of bands %d"%(len(bands), numBands)
    # PLAN: for each band, spit out a hash value
    for bandID, band in enumerate(bands):
        hashedBand=hash(band) #TODO: this is probably unnecessary, given that Apache Spark will hash keys
        # PLAN: yield the bandID and the hash value as a key and the item id as the value
        yield ((bandID, hashedBand), itemID)


if __name__=="__main__":

    logging.basicConfig(level=logging.INFO)
    logging.disable(level=logging.CRITICAL)

    #Below are the arbitrary constants
    seed=89
    desiredSimilarity = 0.5
    numMembersJaccard=120
    numBandsJaccard=40
    numRowsJaccard=3
    numMembersCosine=100
    numBandsCosine=5
    numRowsCosine=20

    # PLAN: collect commandline arguments that refer to input and output filenames, support, and the case number
    inputFilename, similarityMethod, outputFilename = sys.argv[1:]

    # PLAN: create the spark configuration and context objects, and read the input file
    conf = SparkConf().setAppName("Task1").setMaster("local[*]") #TODO: add local[*] to use all cores
    sc = SparkContext(conf=conf)
    sc.setLogLevel("WARN")

    # PLAN: start timer
    startTime=time.time()

    # PLAN: read the input file
    inputRDD=sc.textFile(inputFilename)\
                .map(lambda x:x.split(","))\
                .filter(lambda x:x[0]!='user_id') #TODO: filter out header line

    # PLAN: build a dictionary that translates user IDs into row numbers
    usersWithIndicesRDD=inputRDD.map(lambda x:x[0])\
        .distinct()\
        .zipWithIndex()
    userIDToIndex=usersWithIndicesRDD.collectAsMap()

    #logging.info("Here are some elements from usersWithIndicesRDD: {0}".format(usersWithIndicesRDD.take(5)))
    #logging.info("Length of the user dictionary: %d"%len(userIDToIndex))
    # PLAN: translate user IDs into row numbers
    translatedToIndicesRDD=inputRDD.map(lambda x: (x[1], (userIDToIndex.get(x[0]), x[2])))
    #logging.info("Here are some elements from translatedToIndicesRDD: {0}".format(translatedToIndicesRDD.take(5)))

    # PLAN: build an RDD that has business IDs as keys and a list of tuples of the format (row number, rating)
    # (this represents the characteristic matrix)
    characteristicMatrixRDD = translatedToIndicesRDD.groupByKey()
    #logging.info("Here are some elements from characteristicMatrixRDD: {0}".format(characteristicMatrixRDD.take(5)))


    '''
    for businessRecord in characteristicMatrixRDD.take(5):
        logging.info("For the business {0}...".format(businessRecord[0]))
        for userRecord in businessRecord[1]:
            logging.info("There is user record {0}".format(userRecord))
    '''

    # PLAN: apply an LSH family of functions to build the signature matrix
    if(similarityMethod=='jaccard'):
        numBuckets=10**10
        minHashFamily=createJaccardLSHFamily(numMembersJaccard, numBuckets, seed)
        signatureMatrixRDD = characteristicMatrixRDD.map(lambda vector:createMinHashSignature(vector, minHashFamily))
    else:
        numDimensions=len(userIDToIndex)
        hyperplaneFamily=createCosineLSHFamily(numMembersCosine, numDimensions, seed)
        signatureMatrixRDD = characteristicMatrixRDD.map(lambda vector:createCosineLSHSignature(vector, hyperplaneFamily))

    #logging.info("Here are some elements from signatureMatrixRDD: {0}".format(signatureMatrixRDD.take(5)))
    # PLAN: use the banding technique, or some other permutation of AND/OR conjunctions to build
    # a sufficiently discerning LSH family of functions
    if similarityMethod=='jaccard':
        numBands=numBandsJaccard
        numRows=numRowsJaccard
    else:
        numBands=numBandsCosine
        numRows=numRowsCosine
    bandedRDD = signatureMatrixRDD.flatMap(lambda itemIDAndSig:banding(itemIDAndSig, numBands, numRows))
    logging.info(f"Here is the length of bandedRDD {bandedRDD.count()}")
    #logging.info("Here are some elements from bandedRDD: {0}".format(bandedRDD.take(5)))

    combinedBandsRDD = bandedRDD.groupByKey()
    logging.info(f"Here is the length of combinedBandsRDD {combinedBandsRDD.count()}")
    #logging.info("Here are some elements from combinedBandsRDD: {0}".format(combinedBandsRDD.take(5)))

    '''
    for combinedBand in combinedBandsRDD.take(5):
        logging.info("For the band {0}...".format(str(combinedBand[0])))
        for business in combinedBand[1]:
            logging.info("There is business {0}".format(business))'''

    candidatePairs = combinedBandsRDD.filter(lambda x:len(x[1])>=2)\
        .map(lambda x:set(itertools.combinations(x[1], 2)))\
        .reduce(lambda a,n:a.union(n))
    logging.info(f"Length of candidate pairs: {len(candidatePairs)}")

    # PLAN: check the similarity of these candidate pairs against the original dataset
    trulySimilarPairs=list()
    if similarityMethod=='jaccard':
        jaccardCharacteristicRDD=characteristicMatrixRDD.map(lambda x:(x[0], set([y[0] for y in x[1]])))
        businessIDToUsers=jaccardCharacteristicRDD.collectAsMap()
        for candidatePair in candidatePairs:
            '''
            candidate0=jaccardCharacteristicRDD.filter(lambda x:x[0]==candidatePair[0]).collect()[0]
            candidate1=jaccardCharacteristicRDD.filter(lambda x:x[0]==candidatePair[1]).collect()[0]
            '''
            business0Users=businessIDToUsers[candidatePair[0]]
            business1Users=businessIDToUsers[candidatePair[1]]
            candidateJaccard=computeJaccardSimilarity(business0Users, business1Users)
            if candidateJaccard>=desiredSimilarity:
                sortedPair=sorted(candidatePair)
                trulySimilarPairs.append((sortedPair[0], sortedPair[1], candidateJaccard))
    else:
        #first, preprocess the data to make cosine similarity calculations easier
        cosineCharacteristicRDD=characteristicMatrixRDD.map(lambda x:(x[0],
                                                                      ({userIndex:rating for userIndex, rating in x[1]},
                                                                       frozenset([userIndex for userIndex,_ in x[1]])
                                                                       )
                                                                      )
                                                            )
        businessIDToVectors=cosineCharacteristicRDD.collectAsMap()
        for candidatePair in candidatePairs:
            business0Vector=businessIDToVectors[candidatePair[0]]
            business1Vector=businessIDToVectors[candidatePair[1]]
            candidateCosine=computeCosineSimilarity(business0Vector, business1Vector)
            if candidateCosine>=desiredSimilarity:
                sortedPair=sorted(candidatePair)
                trulySimilarPairs.append((sortedPair[0], sortedPair[1], candidateCosine))

    logging.info(f"Length of truly similar pairs: {len(trulySimilarPairs)}")
    # PLAN: sort within the pairs and then sort among the pairs
    trulySimilarPairs.sort(key=itemgetter(0,1))


    # PLAN: output results to a file
    with open(outputFilename, 'w') as outputFile:
        outputFile.write("business_id_1, business_id_2, similarity")
        for pair in trulySimilarPairs:
            outputFile.write("\n")
            outputFile.write(",".join(str(value) for value in pair))


    # PLAN: stop timer
    endTime=time.time()
    totalTime=endTime-startTime
    print(f"Duration: {totalTime}")