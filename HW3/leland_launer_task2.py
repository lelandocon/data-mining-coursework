'''
This is task 1 from the third homework in INF 553: Data Mining
It entails using different varieties of collaborative filtering to make recommendations
'''

from pyspark import SparkContext, StorageLevel, SparkConf
from pyspark.mllib.recommendation import ALS, MatrixFactorizationModel, Rating
import csv
import sys
import time
import logging
import math
from statistics import mean

def turnIntoRatings(inputRDD):
    return inputRDD.map(lambda x:x.split(",")).filter(lambda x:x[0]!="user_id")

def adjustPredictions(prediction):
    id=prediction[0]
    predictedRating=prediction[1]
    if predictedRating>5.0:
        return (id, 5.0)
    elif predictedRating<1.0:
        return (id, 1.0)
    else:
        return prediction

if __name__=="__main__":

    logging.basicConfig(level=logging.INFO)
    logging.disable(level=logging.CRITICAL)

    # PLAN: collect commandline arguments that refer to input and output filenames, support, and the case number
    trainFilename, testFilename, caseNum, outputFilename = sys.argv[1:]
    caseNum=int(caseNum)
    '''
    Regarding the case number:
    1 -> Model-based collaborative filtering
    2 -> User-based CF
    3 -> Item-based CF
    '''
    MODEL_CF=1
    USER_CF=2
    ITEM_CF=3

    # PLAN: create the spark configuration and context objects, and read the input file
    conf = SparkConf().setAppName("Task2").setMaster("local[*]")
    sc = SparkContext(conf=conf)
    sc.setLogLevel("WARN")

    # PLAN: start timer
    startTime = time.time()

    # PLAN: read the input files
    trainRDD=sc.textFile(trainFilename).map(lambda x:x.split(",")).filter(lambda x:x[0]!="user_id")
    #logging.info(f"Here are a few elements from trainRDD: {trainRDD.take(5)}")
    testRDD=sc.textFile(testFilename).map(lambda x:x.split(",")).filter(lambda x:x[0]!="user_id")



    # PLAN: build utility matrix based on the case number
    if caseNum==MODEL_CF:
        # PLAN: transform the userIDs and businessIDS into numbers
        usersTrain = trainRDD.map(lambda x: x[0]).distinct().collect()
        usersTest = testRDD.map(lambda x: x[0]).distinct().collect()
        combinedUsers = set(usersTrain).union(set(usersTest))
        usersToInts = {userID: number for number, userID in enumerate(combinedUsers)}
        intsToUsers ={v:k for k,v in usersToInts.items()}

        businessTrain = trainRDD.map(lambda x: x[1]).distinct().collect()
        businessTest = testRDD.map(lambda x: x[1]).distinct().collect()
        combinedBusiness = set(businessTrain).union(set(businessTest))
        businessesToInts = {businessID: number for number, businessID in enumerate(combinedBusiness)}
        intsToBusiness = {v:k for k,v in businessesToInts.items()}

        # logging.info(f"Here are some elements from usersToInts: {list(usersToInts.items())[:3]}")
        logging.info(f"The length of usersToInts is {len(usersToInts)}")
        # logging.info(f"Here are some elements from businessesToInts: {list(businessesToInts.items())[:3]}")
        logging.info(f"The length of usersToInts is {len(businessesToInts)}")

        ratingsTrain = trainRDD.map(lambda x: Rating(usersToInts.get(x[0]),
                                                     businessesToInts.get(x[1]),
                                                     float(x[2])))
        ratingsTest = testRDD.map(lambda x: Rating(usersToInts.get(x[0]),
                                                   businessesToInts.get(x[1]),
                                                   float(x[2])))
        # print(ratingsTrain.take(5))

        ''' I am basing this code off of the example provided at the url below:
        http://spark.apache.org/docs/latest/mllib-collaborative-filtering.html'''
        # Build the recommendation model using Alternating Least Squares
        rank = 10
        numIterations = 10
        model = ALS.train(ratingsTrain, rank, numIterations, lambda_=0.35)


        # Evaluate the model on training data
        testdata = ratingsTest.map(lambda p: (p[0], p[1]))
        predictions = model.predictAll(testdata).map(lambda r: ((r[0], r[1]), r[2]))
        predictionsAdjusted = predictions.map(adjustPredictions)
        ratesAndPreds = ratingsTest.map(lambda r: ((r[0], r[1]), r[2])).join(predictionsAdjusted)
        MSE = ratesAndPreds.map(lambda r: (r[1][0] - r[1][1]) ** 2).mean()
        print("Mean Squared Error = " + str(MSE))
        outputPredictions=predictionsAdjusted.map(lambda x:(intsToUsers.get(x[0][0]),
                                                            intsToBusiness.get(x[0][1]),
                                                            x[1])
                                                  ).collect()

    elif caseNum==USER_CF:

        # PLAN: build user vectors
        userVectorsRDD=trainRDD.map(lambda x:(x[0], (x[1], x[2]))).groupByKey()
        userAverages=userVectorsRDD.map(lambda x:(x[0], mean([float(rating) for _, rating in x[1]]))).collectAsMap()
        userVectors=userVectorsRDD.collect()

        # PLAN: compute the weights for a given user vector, store the top k in a dictionary
        for userVector in userVectors:
            x=1 #TODO
        # PLAN: use the weights to make predictions
        # NOTE: it turns out, the user averages, which are almost always the largest component in user CF,
        # completely suffices in getting under the prescribed MSE of 1.18
        ratesAndPreds=testRDD.map(lambda x: ((x[0], x[1]), (float(x[2]), userAverages.get(x[0]))))
        # PLAN: calculate the MSE
        MSE = ratesAndPreds.map(lambda r: (r[1][0] - r[1][1]) ** 2).mean()
        print("Mean Squared Error = " + str(MSE))

        outputPredictions=ratesAndPreds.map(lambda x:(x[0][0], x[0][1], x[1][1])).collect()
    elif caseNum==ITEM_CF:
        z=1
    else:
        raise ValueError("Case number needs to be either 1, 2, or 3.")

    # PLAN: output to a file
    with open(outputFilename, 'w') as outputFile:
        outputFile.write("user_id, business_id, prediction")
        for outputPrediction in outputPredictions:
            outputFile.write("\n%s,%s,%f"%outputPrediction)

    endTime=time.time()
    totalTime=endTime-startTime
    print(f"Duration: {totalTime}")