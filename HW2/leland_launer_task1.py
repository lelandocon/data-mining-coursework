'''
This is task 1 from the second homework in INF 553: Data Mining
It prompts the finding of frequent itemsets via the SON algorithm
'''

from pyspark import SparkContext, SparkConf
import csv
import sys
import time
import logging
import itertools
import os


def addItemToMarketBasket(marketBaskets, key, value):
    '''
    Adds item into a market basket. This will mutate marketBaskets.
    :param marketBaskets: a dict whose keys are individual numbers and whose values are sets of numbers
    :param key: the key whose set may be modified
    :param value: the item to add to the set
    :return: nothing
    '''

    if key not in marketBaskets:
        marketBaskets[key]=set([value])
    else:
        marketBaskets[key].add(value)

def candidateInBasket(candidate, basket):
    candidateInBasket=True
    if len(candidate)>len(basket):
        candidateInBasket=False
    else:
        for item in candidate:
            if item in basket:
                continue
            else:
                candidateInBasket=False
                break
    return candidateInBasket

def countCandidatesForBasketWrapper(candidatesForEntireDataset):
    def countCandidatesForBasket(basket):
        '''
        creates counts for candidates that appear in a single basket.
        Meant to be used in conjunction with flatMap
        :param candidatesForEntireDataset: a list of itemsets
        :return: a list of where each element is an itemset and 1
        e.g. an element could be (('98','87', '26'), 1)
        '''
        logging.basicConfig(level=logging.INFO)
        logging.disable(level=logging.CRITICAL)
        logging.info("Inside the count candidates function")
        candidatesCountPairs=list()
        for candidate in candidatesForEntireDataset:
            if candidateInBasket(candidate, basket):
                candidatesCountPairs.append((candidate,1))
        return candidatesCountPairs
    return countCandidatesForBasket

def createItemsetDictForOutput(inputRDD):
    '''
    creates a dictionary that will facilitate creation
    of output file
    :param inputRDD: the RDD that has itemsets in the form (itemset, 1)
    :return: a dictionary whose keys are the level of cardinality
    and whose values are lists of itemsets of that cardinality
    '''
    itemsetDict=dict()
    sortedItemsetsAndCardinalities = inputRDD.map(formatIndividualItemsetAsStr).collect()
    for itemset, cardinality in sortedItemsetsAndCardinalities:
        if cardinality not in itemsetDict:
            itemsetDict[cardinality]=[itemset]
        else:
            itemsetDict[cardinality].append(itemset)
    for cardinality in itemsetDict.keys():
        itemsetDict[cardinality]=sorted(itemsetDict[cardinality])
    return itemsetDict

def formatIndividualItemsetAsStr(itemsetAndCount):
    itemset, _ = itemsetAndCount
    lenItem = len(itemset)
    if lenItem == 1:
        for member in itemset:
            return ("(\'%s\')"%str(member), lenItem)
    else:
        return (str(tuple(sorted(itemset))), lenItem)

def writeItemsetDict(outputFile, itemsetDict):
    for key in sorted(itemsetDict.keys()):
        outputFile.write(",".join(itemsetDict[key]) + "\n")

def createValidCandidates(freqInSampleSingletons, freqInSampleItemsets):
    '''
    Create valid candidate itemsets of a cardinality one degree higher
    than the input. Do so by combining singletons with itemsets.
    :param freqInSampleSingletons: a list of singletons, which are strings
    :param freqInSampleItemsets: a list of itemsets, which are tuples of strings
    :return: a set of tuples of strings
    '''
    validCandidates = set()
    if len(freqInSampleItemsets)==0:
        pass
    else:
        allCandidates=itertools.product(freqInSampleSingletons, freqInSampleItemsets)
        for candidate in allCandidates:
            if candidate[0] not in candidate[1]:
                candidateCanonicalFormat=tuple(sorted((candidate[0],)+candidate[1]))
                validCandidates.add(candidateCanonicalFormat)
    return validCandidates

def createValidCandidatesV2(freqInSampleItemsets, cardinalityOfOldItemsets):
    '''
    Create valid candidate itemsets of a cardinality one degree higher
    than the input. Do so by combining the itemsets with themselves
    :param freqInSampleItemsets: a list of itemsets, which are tuples of strings
    :param cardinalityOfOldItemsets: a int that specifies the cardinality of the itemsets in freqInSampleItemsets
    :return: a set of frozen sets of strings. Frozen sets were chosen here so that
    they may serve as dictionary keys elsewhere
    '''
    validCandidates = set()
    allCandidates=itertools.combinations(freqInSampleItemsets, 2)
    for candidate in allCandidates:
        firstSegment=frozenset(candidate[0])
        secondSegment=frozenset(candidate[1])
        candidateCanonicalFormat = firstSegment.union(secondSegment)
        if len(candidateCanonicalFormat)==cardinalityOfOldItemsets+1:
            validCandidates.add(candidateCanonicalFormat)
    return validCandidates

def oldWayOfCheckingMembership(candidate, element):
    '''
    This is the old, inefficient way to check if the candidate
    is in a basket/element
    :param candidate: a frozen set with the candidate's members
    :param element: a list of items in a basket
    :return: boolean value indicating whether all the members of the candidate are in the basket
    '''
    if len(candidate) > len(element):
        return False # a candidate cannot possibly be present in an element that it is larger than
    # logging.info("Current candidate: %s"%str(candidate))
    allItemsInElement = True
    for item in candidate:
        # logging.info("Current item: %s"%item)
        if item in element:
            pass
        else:
            allItemsInElement = False
            # logging.info("This candidate %s is not in this element %s"%(candidate, element))
    return allItemsInElement

def aPrioriWrapper(support):
    def aPriori(elementsOfData):
        '''
        Produces candidates of frequent itemsets based on A-priori algorithm
        :param elementsOfPartition: an iterable of baskets which hold items (likely a list of lists)
        :return: a list of the candidate frequent itemsets
        '''
        logging.basicConfig(level=logging.INFO)
        logging.disable(level=logging.CRITICAL)
        freqInSample=dict()
        elementsOfData=[frozenset(element) for element in elementsOfData]
        #logging.info('Elements of data: %s'%elementsOfData)
        logging.info("The type of elementsOfData:%s"%type(elementsOfData))
        # PLAN: first, obtain the singletons that are frequent in this sample
        singletonCounts=dict()
        for element in elementsOfData:
            for item in element:
                if item not in singletonCounts:
                    singletonCounts[item]=1
                else:
                    singletonCounts[item]+=1
        freqInSampleSingletons=[frozenset([singleton]) for singleton, count in singletonCounts.items() if count>=support]
        cardinalityOfItemset=1
        logging.info("Length of frequent singletons: %d"%(len(freqInSampleSingletons)))
        freqInSample[cardinalityOfItemset]=freqInSampleSingletons

        # PLAN: now, produce candidate itemsets of greater cardinality
        currentCandidates=createValidCandidatesV2(freqInSampleSingletons, cardinalityOfItemset)
        cardinalityOfItemset+=1
        #logging.info("Here are the current candidates: %s"%currentCandidates)
        logging.info("Length of current candidates of cardinality %d: %d"%(cardinalityOfItemset, len(currentCandidates)))
        findCandidatesOfGreaterCardinality(elementsOfData, currentCandidates, freqInSample, cardinalityOfItemset, support)
        for itemsetsOfSameSize in freqInSample.values():
            for itemset in itemsetsOfSameSize:
                yield (itemset, 1)
    return aPriori

def pcyWrapper(support):
    def pcy(elementsOfData):
        '''
        Produces candidates of frequent itemsets based on  algorithm
        :param elementsOfPartition: an iterable of baskets which hold items (likely a list of lists)
        :return: a list of the candidate frequent itemsets
        '''
        logging.basicConfig(level=logging.INFO)
        logging.disable(level=logging.CRITICAL)
        freqInSample = dict()
        elementsOfData = [frozenset(element) for element in elementsOfData]
        # logging.info('Elements of data: %s'%elementsOfData)
        logging.info("The type of elementsOfData:%s" % type(elementsOfData))
        # PLAN: first, obtain the singletons that are frequent in this sample
        singletonCounts, pairCounts = dict(), dict()
        for element in elementsOfData:
            for item in element:
                if item not in singletonCounts:
                    singletonCounts[item] = 1
                else:
                    singletonCounts[item] += 1
            pairsInElement=itertools.combinations(element, 2)
            for pair in pairsInElement:#pair is in format tuple(str, str)
                canonicalPair=frozenset(pair) #canonical format is frozenset(two items)
                hashedPair=customHash1(canonicalPair)
                if hashedPair not in pairCounts:
                    pairCounts[hashedPair]=1
                else:
                    pairCounts[hashedPair]+=1

        freqInSampleSingletons = [frozenset([singleton]) for singleton, count in singletonCounts.items() if
                                  count >= support]
        cardinalityOfItemset = 1
        logging.info("Length of frequent singletons: %d" % (len(freqInSampleSingletons)))
        freqInSample[cardinalityOfItemset] = freqInSampleSingletons

        # PLAN: now, produce candidate itemsets of greater cardinality
        initialCandidates = createValidCandidatesV2(freqInSampleSingletons, cardinalityOfItemset)
        pcyCandidates = list()
        for candidate in initialCandidates:
            hashedCandidate=customHash1(candidate)
            if hashedCandidate in pairCounts and pairCounts[hashedCandidate]>=support:
                pcyCandidates.append(candidate)
            else: #a pair of frequent singletons may have never occurred in the same basket, so they'll end up here
                pass
        currentCandidates=pcyCandidates
        cardinalityOfItemset += 1
        # logging.info("Here are the current candidates: %s"%currentCandidates)
        logging.info("Length of initial candidates of cardinality %d: %d"%(cardinalityOfItemset, len(initialCandidates)))
        logging.info(
            "Length of current candidates of cardinality %d: %d" % (cardinalityOfItemset, len(currentCandidates)))
        findCandidatesOfGreaterCardinality(elementsOfData, currentCandidates, freqInSample, cardinalityOfItemset, support)
        for itemsetsOfSameSize in freqInSample.values():
            for itemset in itemsetsOfSameSize:
                yield (itemset, 1)
    return pcy

def customHash1(itemset):
    return hash(itemset) % 524288 #2^19=524288

def findCandidatesOfGreaterCardinality(elementsOfData, candidatePairs, freqInSample, cardinalityOfItemset, supportPerSample):
    '''
    Finds candidate itemsets of cardinality greater than 2. DOES NOT RETURN ANYTHING, BUT MODIFIES FREQINSAMPLE
    :param elementsOfData: list of frozen sets of market baskets
    :param candidatePairs: list of frozen sets of size 2
    :param freqInSample: the dictionary whose keys are cardinalities and whose values are lists of frozen candidates
    of that particular cardinality
    :param cardinalityOfItemset:
    :return: NOTHING
    '''
    logging.basicConfig(level=logging.INFO)
    logging.disable(level=logging.CRITICAL)
    currentCandidates = candidatePairs
    while len(currentCandidates) != 0:
        # PLAN: find the counts for the current candidates
        candidateCounts = dict()
        for element in elementsOfData:
            # logging.info("-------------------------CURRENT ELEMENT--------------------------------")
            # logging.info("Element: %s"%str(element))
            for candidate in currentCandidates:
                if candidate.issubset(element):
                    if candidate in candidateCounts:
                        candidateCounts[candidate] += 1
                    else:
                        candidateCounts[candidate] = 1
        # PLAN: find the itemsets that are frequent in this sample
        # logging.info("Here are the counts for the candidate itemsets of cardinality %d: %s"%(cardinalityOfItemset, candidateCounts))
        freqInSampleItemsets = [candidate for candidate, count in candidateCounts.items() if count >= supportPerSample]
        if len(freqInSampleItemsets) == 0:
            break
        freqInSample[cardinalityOfItemset] = freqInSampleItemsets
        # logging.info("Here are the frequent itemsets of cardinality %d: %s" % (cardinalityOfItemset, freqInSampleItemsets))

        # PLAN: use these "frequent in sample" itemsets and the "frequent in sample" singletons
        # to create candidates with cardinality one degree higher
        currentCandidates = createValidCandidatesV2(freqInSampleItemsets, cardinalityOfItemset)
        cardinalityOfItemset += 1
        # logging.info("Here are the current candidates: %s" % currentCandidates)
        logging.info("Length of current candidates of cardinality %d : %d" % (cardinalityOfItemset, len(currentCandidates)))

def sonAlgorithmWriteToFile(basketsRDD, support, outputFilename, freqAlgoWrapper):
    # PLAN: execute SON algorithm
    # PLAN: first, generate the candidate pairs by running A-priori, PCY, Multihash or Multipass on each partition
    logging.disable(level=logging.CRITICAL)
    numberOfSamples = basketsRDD.getNumPartitions()
    supportPerSample = support / numberOfSamples
    logging.info("Number of partitions/samples: %d" % numberOfSamples)
    logging.info("Support per sample %f" % supportPerSample)
    candidatesRDD = basketsRDD.mapPartitions(freqAlgoWrapper(supportPerSample))

    # PLAN: second, reduce the rdd to eliminate duplicate candidate pairs
    candidatesForEntireDatasetRDD = candidatesRDD.reduceByKey(lambda a, n: a)
    logging.info("Size of candidatesRDD: %d" % candidatesRDD.count())
    logging.info("Size of candidatesForEntireDatasetRDD: %d" % candidatesForEntireDatasetRDD.count())
    candidatesForEntireDataset = candidatesForEntireDatasetRDD.map(lambda x: x[0]).collect()

    # PLAN: third, count the candidate pairs in the data
    candidateCountsIsolated = basketsRDD.flatMap(countCandidatesForBasketWrapper(candidatesForEntireDataset))

    # PLAN: fourth, sum the counts with a reduce function
    candidateCountsSummed = candidateCountsIsolated.reduceByKey(lambda a, n: a + n)

    # PLAN: fifth, filter out all those that are below the support threshold
    frequentItemsetsRDD = candidateCountsSummed.filter(lambda x: x[1] > support)
    logging.info("Size of frequent itemsets: %d" % frequentItemsetsRDD.count())

    # PLAN: sort the candidate itemsets and frequent itemsets lexicographically
    # frequentItemsets = frequentItemsetsRDD.map(lambda x: tuple(sorted(x[0])))

    # PLAN: create output-ready versions of the candidate and frequent itemsets:
    candidateOutput = createItemsetDictForOutput(candidatesForEntireDatasetRDD)
    frequentOutput = createItemsetDictForOutput(frequentItemsetsRDD)
    logging.info("Length of frequent output: %d" % len(frequentOutput))

    # PLAN: write candidate itemsets and frequent itemsets to output file
    with open(outputFilename, 'w') as outputFile:
        outputFile.write("Candidates:\n")
        writeItemsetDict(outputFile, candidateOutput)
        outputFile.write("\nFrequent itemsets:\n")
        writeItemsetDict(outputFile, frequentOutput)

if __name__=="__main__":

    logging.basicConfig(level=logging.INFO)
    #logging.disable(level=logging.CRITICAL)

    # PLAN: collect commandline arguments that refer to input and output filenames, support, and the case number
    caseNum, support, inputFilename, outputFilename = sys.argv[1:]
    caseNum=int(caseNum)
    support=int(support)
    assert caseNum==1 or caseNum==2

    # PLAN: create the spark configuration and context objects, and read the input file

    sc = SparkContext(master="local", appName="Task1")
    sc.setLogLevel("WARN")

    CHOSEN_ALGO_WRAPPER = aPrioriWrapper

    # PLAN: start timer, preprocess CSV and place data into RDD
    startTime=time.time()
    marketBaskets=dict()
    with open(inputFilename) as inputFile:
        reader=csv.DictReader(inputFile)
        for row in reader:
            if caseNum==1: #case 1: checking for frequent businesses
                addItemToMarketBasket(marketBaskets, row['user_id'], row['business_id'])
            elif caseNum==2: #case 2: checking for frequent users
                addItemToMarketBasket(marketBaskets, row['business_id'], row['user_id'])

    for key in marketBaskets.keys(): #TODO: edit out this loop and examine how it affects execution
        marketBaskets[key]=list(marketBaskets[key])

    baskets = list(marketBaskets.values())
    #print(marketBaskets)

    # PLAN: execute SON algorithm
    # PLAN: first, generate the candidate pairs by running A-priori, PCY, Multihash or Multipass on each partition
    basketsRDD=sc.parallelize(baskets)
    sonAlgorithmWriteToFile(basketsRDD, support, outputFilename, CHOSEN_ALGO_WRAPPER)

    # PLAN: end timer, display duration in console
    endTime = time.time()
    totalTime = endTime - startTime
    print("Duration: %f" % totalTime)
