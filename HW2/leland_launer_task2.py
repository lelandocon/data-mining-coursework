from pyspark import SparkContext, StorageLevel, SparkConf
import csv
import sys
import time
import logging
import itertools
import os
import io
from leland_launer_task1 import sonAlgorithmWriteToFile, pcyWrapper, aPrioriWrapper

def transformInputRDDIntoBaskets(line):
    separatedLine=line.split(',')
    transactionID=separatedLine[0]
    customerID=separatedLine[1]
    productID=separatedLine[5][1:-1]
    return ("{0}-{1}".format(transactionID, customerID), productID)

if __name__=="__main__":

    logging.basicConfig(level=logging.INFO)
    logging.disable(level=logging.CRITICAL)

    # PLAN: collect commandline arguments that refer to input and output filenames, support, and the case number
    filter_thres, support, inputFilename, outputFilename = sys.argv[1:]
    filter_thres=int(filter_thres)
    support=int(support)

    # PLAN: create the spark configuration and context objects, and read the input file
    conf = SparkConf().setAppName("Task2").setMaster("local[*]")
    sc = SparkContext(conf=conf)
    sc.setLogLevel("WARN")

    CHOSEN_ALGO_WRAPPER=pcyWrapper

    # PLAN: start timer
    startTime=time.time()

    # PLAN: form list of incomplete baskets, feed them into RDD
    '''
    incompleteBaskets=list()
    with io.open(inputFilename, "r", encoding="utf-8-sig") as inputFile:
        reader = csv.DictReader(inputFile)
        for row in reader:
            newBasket=("{0}-{1}".format(row['TRANSACTION_DT'], row['CUSTOMER_ID']), row['PRODUCT_ID'])
            incompleteBaskets.append(newBasket)
    incompleteBasketsRDD = sc.parallelize(incompleteBaskets, numSlices=200)
    print(incompleteBasketsRDD.take(2))
    '''
    # TODO: account for the headers, though they will almost always be filtered out later
    inputRDD = sc.textFile(inputFilename)
    incompleteBasketsRDD = inputRDD.map(transformInputRDDIntoBaskets)
    keysAndBasketsRDD = incompleteBasketsRDD.groupByKey()
    allBasketsRDD = keysAndBasketsRDD.map(lambda x:x[1])
    basketsRDD = allBasketsRDD.filter(lambda x:len(x)>filter_thres)

    logging.info("Incomplete baskets count: %d"%incompleteBasketsRDD.count())
    logging.info("keyAndBaskets count: %d" % keysAndBasketsRDD.count())
    logging.info("All baskets count: %d" % allBasketsRDD.count())
    logging.info("Filtered baskets count: %d" % basketsRDD.count())
    lenBasketsRDD=basketsRDD.map(lambda x:len(x))
    lenBaskets=lenBasketsRDD.reduce(lambda a,n: a+n)
    avgLenBaskets=lenBaskets/basketsRDD.count()
    logging.info("Average length of a basket: %f"%avgLenBaskets)

    # PLAN: execute SON algorithm
    sonAlgorithmWriteToFile(basketsRDD, support, outputFilename, CHOSEN_ALGO_WRAPPER)


    # PLAN: end timer, display duration in console
    endTime = time.time()
    totalTime = endTime - startTime
    print("Duration: %f" % totalTime)

