'''
This is task 2 from the 5th homework in INF 553: Data Mining
It consists of using the Flajolet-Martin algorithm to estimate
unique elements in a stream (specifically the cities seen in Yelp business data)
'''

from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
import sys
import time
import json
import logging
import binascii
import random
from functools import partial
from statistics import mean, median


def strToIntHash(aStr):
    return int(binascii.hexlify(aStr.encode("utf8")), 16)

def standardHash(numberToHash, a, b, numBuckets):
    return (a*numberToHash+b)%numBuckets

def createHashFunctions(numHashFunctions, numBuckets, seed):
    random.seed(seed)
    newHashFamily=list()
    for i in range(numHashFunctions):
        a = random.randint(1, numBuckets - 1)
        b = random.randint(0, 10 ** 9)  # ARBITRARY CONSTANT
        # logging.info(f"a is {a} and b is {b}")
        newHashFunc = partial(standardHash, a=a, b=b, numBuckets=numBuckets)
        newHashFamily.append(newHashFunc)
    for i in range(len(newHashFamily) - 1):
        assert id(newHashFamily[i]) != id(newHashFamily[i + 1])
    return newHashFamily

def findTrailingZeroes(anInt):
    #logging.info(f"Finding zeroes for {anInt}, with binary rep {bin(anInt)}")
    trailingZeroes=0
    if anInt!=0:
        while anInt>>trailingZeroes&1==0:
            trailingZeroes+=1
    return trailingZeroes

def createTrailingZeroPairs(elem):
    '''
    Creates pairs of the type (hashFuncIndex, numTrailingZeroes)
    MEANT TO BE USED IN CONJUNCTION WITH FLATMAP
    :param elem: a string of the value to be hashed
    :yield: a pair of the type (hashFuncIndex, numTrailingZeroes)
    '''
    global hashFunctions

    # PLAN: hash the string to an int
    elemInt=strToIntHash(elem)

    # PLAN: iterate over hash functions
    for hashFuncIndex, hashFunc in enumerate(hashFunctions):
        # PLAN: hash the int with the given hash function
        hashedVal=hashFunc(elemInt)
        # PLAN: find the trailing zeroes
        numTrailingZeroes=findTrailingZeroes(hashedVal)
        # PLAN: yield the pair
        yield (hashFuncIndex, numTrailingZeroes)

def calculateMedianOfAverages(listOf2ToR, groupSize):
    averages = list()
    for i in range(0, len(hashFunctions), groupSize):
        certainGroup = listOf2ToR[i:i + groupSize]
        averages.append(mean(certainGroup))
    return median(averages)

def updateGlobalMaxTrailingZeroes(batchMaxTrailingZeroes):
    global globalMaxTrailingZeroes
    assert len(globalMaxTrailingZeroes)==len(batchMaxTrailingZeroes), "The max trailing zeroes arrays do not match up"
    newMaxTrailingZeroes=[max(globalMaxTrailingZeroes[i], batchMaxTrailingZeroes[i])
                          for i in range(len(globalMaxTrailingZeroes))]
    globalMaxTrailingZeroes=newMaxTrailingZeroes

def flajoletMartin(time, rdd, outputFile, groupSize, local):
    '''
    Runs Flajolet-Martin on an RDD from a DStream and writes ground truth and estimate of the
    number of unique elements in a given file.
    MEANT TO BE USED IN CONJUNCTION WITH foreachRDD
    :param time: a timestamp associated with the RDD
    :param rdd: an RDD from a DStream whose elements are JSONS
    :param outputFile: a file descriptor for the output file
    :param groupSize: an integer describing the size of the groups in Flajolet-Martin's average/median technique
    :param local: boolean value that indicates whether the algorithm should be run locally (could be faster if
    data is small)
    :return: None
    '''
    global previousCities

    # PLAN: preprocess the data
    cities=rdd.map(json.loads).map(lambda x:x['city'])
    logging.info(f"Preprocessing at {time} complete.")

    # PLAN: run local or distributed versions of finding the batch max trailing zeroes array
    if local:
        logging.info("Going down local branch...")
        batchCities=cities.collect()
        batchMaxTrailingZeroes=flajoletMartinLocal(batchCities)
    else:
        logging.info("Going down distributed branch...")
        batchMaxTrailingZeroes=flajoletMartinDistributed(cities)
    logging.info(f"batchMaxTrailingZeroes:\n{batchMaxTrailingZeroes}")

    # PLAN: update the global max trailing zeroes array
    updateGlobalMaxTrailingZeroes(batchMaxTrailingZeroes)
    logging.info(f"globalMaxTrailingZeroes:\n{globalMaxTrailingZeroes}")

    # PLAN: create list of 2^R based on global max trailing zeroes array
    listOf2ToR = [2 ** (r) for r in globalMaxTrailingZeroes]
    correctionFactor=0.77351 #this correction factor can be found in the original paper and is cited on Wikipedia
    corrected2ToR = [estimate/correctionFactor for estimate in listOf2ToR]
    logging.info(f"listOf2ToR: \n{listOf2ToR}")
    logging.info(f"corrected2ToR: \n{corrected2ToR}")

    # PLAN: split up the hash functions into groups, calculate means and then median
    uniqueElementsEstimate = round(calculateMedianOfAverages(corrected2ToR, groupSize))

    # PLAN : write to output file
    groundTruth=len(previousCities)
    logging.info(f"uniqueElementsEstimate: {uniqueElementsEstimate}")
    logging.info(f"groundTruth: {groundTruth}")
    outputFile.write(f"{time},{groundTruth},{uniqueElementsEstimate}\n")

    return None

def flajoletMartinLocal(batchCities):
    '''
    Gathers the batch's array of maximum trailing zeroes for the hash functions
    on the driver side. This can be quicker if the data is small.
    :param batchCities: a list of strings representing cities
    :return: an list of maximum trailing zeroes indexed by hashFunctionIndex
    '''
    global hashFunctions
    global previousCities
    global globalMaxTrailingZeroes
    # NOTE: hashMaxZeroes will be a 2-D list, ultimately with dimensions H by N
    # where H is the number of hash functions and N is the batch size
    hashMaxZeroes=[list() for i in range(len(hashFunctions))]
    # PLAN: iterate over batch
    for city in batchCities:
        #logging.info(f"For city {city}...")
        # PLAN: update the ground truth
        previousCities.add(city)
        # PLAN: create trailing zero pairs for this batch value
        for hashFuncIndex, numTrailingZeroes in createTrailingZeroPairs(city):
            #logging.info(f"{city} has {numTrailingZeroes} zeroes for hash func {hashFuncIndex}")
            hashMaxZeroes[hashFuncIndex].append(numTrailingZeroes)

    # PLAN: take the max of all the trailing zeroes
    batchMaxTrailingZeroes=[max(zeroList) for zeroList in hashMaxZeroes]
    return batchMaxTrailingZeroes

def flajoletMartinDistributed(cities):
    '''
    Gathers the batch's array of maximum trailing zeroes for the hash functions
    in a distributed fashion. Can scale well for big data.
    :param cities: an RDD whose elements are strings that represent cities
    :return: an list of maximum trailing zeroes indexed by hashFunctionIndex
    '''
    global previousCities
    # PLAN: update the ground truth
    distinctCities = cities.distinct().collect()
    for city in distinctCities:
        previousCities.add(city)
    logging.info(f"Size of previousCities: {len(previousCities)}")

    # PLAN: hash the elements and make new RDD with k,v pairs (hashFuncIndex, numTrailingZeroes)
    trailingZeroPairs = cities.flatMap(createTrailingZeroPairs)
    logging.info(f"trailingZeroPairs: {trailingZeroPairs.take(5)}")

    # PLAN: group by key and reduce, taking the maximum of the trailing zeroes
    maxTrailingZeros = trailingZeroPairs.reduceByKey(lambda a, n: max(a, n))
    logging.info(f"maxTrailingZeros: {maxTrailingZeros.take(5)}")

    # PLAN: update the global max trailing zeroes array
    sortedPairs = sorted(maxTrailingZeros.collect())
    batchMaxTrailingZeroes =[trailingZero for hashFuncIndex, trailingZero in sortedPairs]
    return batchMaxTrailingZeroes



if __name__=="__main__":

    logging.basicConfig(level=logging.INFO)
    logging.disable(level=logging.CRITICAL)

    #Below are the arbitrary constants
    seed=54
    hostName="localhost"
    batchDuration=5
    windowDuration=30
    slideDuration=10
    '''Need to explain the reasoning for numBuckets
    We want to have enough buckets for all the unique elements to be given their own bucket
    and thus fully express the range of unique values. Essentially, we want no collisions.
    I'm estimating the number of unique elements,i.e. the number of unique cities in North America, to be around 10^5. 
    I'll be conservative and make the range of the hash functions longer'''
    numBuckets=10**7
    numHashFunctions=50 #the more hash functions, the better
    numGroups=10
    groupSize=int(numHashFunctions/numGroups)
    local=True

    # PLAN: collect commandline arguments
    portNum, outputFilename = sys.argv[1:]
    portNum=int(portNum)

    # PLAN: create the spark configuration and context objects
    conf = SparkConf().setAppName("Task2, Flajolet-Martin").setMaster("local[*]")
    sc = SparkContext(conf=conf)
    sc.setLogLevel("ERROR")


    # PLAN: start timer
    startTime=time.time()

    # PLAN: open file
    outputFile=open(outputFilename,'w')
    outputFile.write("Time,Ground Truth,Estimation\n")

    # PLAN: create the hash functions that will be used in conjunction with the Bloom filter
    hashFunctions=createHashFunctions(numHashFunctions, numBuckets, seed)

    # PLAN: create previous values set that is essential for tracking the ground truth of unique cities
    previousCities=set()

    # PLAN: create global array of max trailing zeroes for each hash function
    globalMaxTrailingZeroes=[0 for i in range(len(hashFunctions))]

    # PLAN: broadcast these variables
    pc=sc.broadcast(previousCities)
    hf=sc.broadcast(hashFunctions)
    gmtz=sc.broadcast(globalMaxTrailingZeroes)

    # PLAN: receive a batch of data
    ssc = StreamingContext(sc, batchDuration)
    lines = ssc.socketTextStream(hostName, portNum).window(windowDuration, slideDuration)

    # PLAN: run Flajolet-Martin algorithm on streaming data
    lines.foreachRDD(lambda time, rdd:flajoletMartin(time, rdd, outputFile, groupSize, local))

    # PLAN: start and end the socket stream
    ssc.start()
    ssc.awaitTermination()

    # PLAN: close the output file
    outputFile.close()