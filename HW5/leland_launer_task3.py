'''
This is task 3 from the 5th homework in INF 553: Data Mining
It consists of using Fixed Size Sampling/Reservoir Sampling with regards to
tweets from Twitter
'''

import logging
import tweepy
import random

def addTweetToReservoir(newTweet):
    global reservoir
    # PLAN: add the tweet to the reservoir
    reservoir.append(newTweet)
    logging.info(f"Just added these hashtags: {newTweet.entities['hashtags']}")
    return None

def removeRandomTweetFromReservoir():
    global reservoir
    # PLAN: randomly choose tweet to remove
    tweetToRemove=random.choice(reservoir)
    # PLAN: remove tweet from reservoir
    reservoir.remove(tweetToRemove)
    logging.info(f"Just removed these hashtags: {tweetToRemove.entities['hashtags']} ")
    return None

def constructTagCounts():
    global reservoir
    tagCounts=dict()
    # PLAN: iterate over tweets in reservoir
    for tweet in reservoir:
        tweetHashtags=tweet.entities['hashtags']
        for tweetHashtag in tweetHashtags:
            tweetHashtagText=tweetHashtag['text']
            # PLAN: increment the tag count for a given tag
            if tweetHashtagText in tagCounts:
                tagCounts[tweetHashtagText]+=1
            else:
                tagCounts[tweetHashtagText]=1
    return tagCounts

def printTagCounts(tagCounts):
    '''
    Prints the tags with the top 5 frequencies.
    Note: this means that more than 5 tags can be printed, since multiple
    tags can share any of the top 5 frequencies
    :param tagCounts:
    :return:
    '''
    global sequenceNum
    print(f"The number of tweets with tags from the beginning: {sequenceNum}")
    sortedTagsAndCounts=sorted(tagCounts.items(), key=lambda x:(-x[1], x[0]))
    countOfPrevTag=sortedTagsAndCounts[0][1]
    numUniqueCounts=1
    maxUniqueCounts=5
    for tag, count in sortedTagsAndCounts:
        if count==countOfPrevTag:
            print(f"{tag}:{count}")
        elif numUniqueCounts<maxUniqueCounts:
            numUniqueCounts+=1
            countOfPrevTag=count
            print(f"{tag}:{count}")
        else:
            break
    return None

class ReservoirListener(tweepy.StreamListener):
    def on_status(self, status):
        '''
        This will be the main function used to process the tweets
        and reorder the reservoir/sample
        :param status: an object representing a single tweet
        :return: None
        '''
        global sequenceNum
        global maxReservoirSize

        newTweet=status
        # PLAN: check to see if the tweet has any hashtags; if not, ignore it
        newTweetHashtags=newTweet.entities['hashtags']
        if len(newTweetHashtags)!=0:
            # PLAN: increment the global sequence number
            sequenceNum+=1
            # PLAN: check the sequence number; if lower than the max reservoir size, then add tweet to reservoir
            if sequenceNum<=maxReservoirSize:
                addTweetToReservoir(newTweet)
            else:
                # PLAN: if higher than the max reservoir size, then compute the probability of adding the tweet to the reservoir,
                # which should be reservoirSize/sequenceNum
                chanceToIncludeNewTweet=random.randint(1, sequenceNum)
                if chanceToIncludeNewTweet<=maxReservoirSize:
                    # PLAN: if the new tweet will be kept, an old tweet must be removed
                    removeRandomTweetFromReservoir()
                    addTweetToReservoir(newTweet)
                else:
                    # PLAN: if the new tweet will not be kept, do nothing
                    pass
                assert len(reservoir) == maxReservoirSize, f"The reservoir has size {len(reservoir)}"\
                    f" when it should be {maxReservoirSize}"

            # PLAN: print the top tags represented in the reservoir
            tagCounts = constructTagCounts()
            printTagCounts(tagCounts)
        else:
            pass
        return None

if __name__=="__main__":

    logging.basicConfig(level=logging.INFO)
    logging.disable(level=logging.CRITICAL)

    #Below are the arbitrary constants
    consumerKey="jwM5W8PZJcWRrne3w6gLD1rjr"
    consumerSecretKey="RrkDpbFmucQSHBk6TLMJlmJz0EGajvBdsdGnXjJgKDDOswq8mb"
    accessToken="1201945489781841921-ne7cTfdUCEJWwIcJuFOnvTVwmTY6gL"
    accessTokenSecret="4FBeTHIqORp5ja8LVe1xzT47XS2FAgXDRuvDths3wyIXU"
    maxReservoirSize=150
    sequenceNum = 0
    reservoir = list()
    seed=62


    random.seed(seed)

    # PLAN: obtain authorization from Twitter
    #auth = tweepy.AppAuthHandler(consumerKey, consumerSecretKey)
    auth = tweepy.OAuthHandler(consumerKey, consumerSecretKey)
    auth.set_access_token(accessToken, accessTokenSecret)

    # PLAN: get an API object
    api = tweepy.API(auth)

    # PLAN: create instance of ReservoirListener
    theReservoirListener=ReservoirListener()

    # PLAN: start listening to a Twitter stream
    theStream=tweepy.Stream(auth=api.auth, listener=theReservoirListener)
    theStream.sample()