'''
This is task 1 from the 5th homework in INF 553: Data Mining
It entails the construction and maintenance of a Bloom filter
for detecting whether businesses of a particular state have
been seen in the data stream before the current element
'''

from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
import sys
import time
import json
import logging
import binascii
import random
from functools import partial


#global constants and variables
SEEN=3
UNSEEN=4


def strToIntHash(aStr):
    return int(binascii.hexlify(aStr.encode("utf8")), 16)

def hashAndCheck(elem, hashFunctions, bloomArray):
    # PLAN: iterate over hash functions
    for hashFunction in hashFunctions:
        strToInt=strToIntHash(elem) #converts string to int
        bucketID=hashFunction(strToInt)
        # PLAN: for each hash value, check the bloom filter
        seenOrUn=checkBloomFilter(bucketID, bloomArray)
        # PLAN: if any of the values are hashed to an unfilled bucket, declare it as unseen
        if seenOrUn==0:
            return (elem, UNSEEN)
    # PLAN: if all the values are hashed to filled buckets, declare it seen
    return (elem, SEEN)
def checkBloomFilter(bucketID, bloomArray):
    '''
    checks the Bloom filter array at a given position
    :param bucketID: an integer that represents a bucket id/hashed value
    :param bloomArray: an integer that serves as a bit array
    :return: 0 if the bucket is empty, 1 if the bucket is full
    '''

    return (bloomArray>>bucketID)&1

def fillBucketInBloomFilter(bucketID, bloomArray):
    '''
    takes a Bloom filter and fills a given bucket
    :param bucketID: an integer that represents a bucket id/hashed value
    :param bloomArray: an integer that serves as a bit array
    :return: a new bit array with the bucket filled
    '''
    return bloomArray|(1<<bucketID)

def createPartialFilter(elem, hashFunctions):
    partialFilter=0
    for hashFunction in hashFunctions:
        strToInt=strToIntHash(elem)
        bucketID=hashFunction(strToInt)
        partialFilter=fillBucketInBloomFilter(bucketID, partialFilter)
    return partialFilter

def modifyGlobalBloomFilter(newFilterElem):
    global globalBloomArray
    newGlobalBloomArray=globalBloomArray|newFilterElem
    globalBloomArray=newGlobalBloomArray
    return newGlobalBloomArray

def standardHash(numberToHash, a, b, numBuckets):
    return (a*numberToHash+b)%numBuckets

def createHashFunctions(numHashFunctions, numBuckets, seed):
    random.seed(seed)
    newHashFamily=list()
    for i in range(numHashFunctions):
        a = random.randint(1, numBuckets - 1)
        b = random.randint(0, 10 ** 9)  # ARBITRARY CONSTANT
        # logging.info(f"a is {a} and b is {b}")
        newHashFunc = partial(standardHash, a=a, b=b, numBuckets=numBuckets)
        newHashFamily.append(newHashFunc)
    for i in range(len(newHashFamily) - 1):
        assert id(newHashFamily[i]) != id(newHashFamily[i + 1])
    return newHashFamily

def enactBloomFilter(time, rdd, outputFile):
    '''
    Enacts the Bloom Filter algorithm on top of an RDD.
    MEANT TO BE USED IN CONJUNCTION WITH foreachRDD METHOD OF DStream object
    :param time: the string timestamp associated with a given RDD/batch
    :param rdd: a batch in RDD form
    :return: None
    '''
    global previousStates
    global globalBloomArray
    global hashFunctions

    #print(f"This is the type of rdd: {type(rdd)}\n")
    # PLAN: do preprocessing, if necessary
    businessJsons = rdd.map(json.loads)
    states = businessJsons.map(lambda x: x['state'])
    states.cache()
    batchSize=states.count()
    logging.info(f"Batch size at {time} is {batchSize}\n")
    logging.info(f"Batch is {states.collect()}\n")

    # PLAN: use Bloom Filter to predict seen and unseen values
    statesSeenAndUnseen = states.map(lambda x: hashAndCheck(x, hashFunctions, globalBloomArray))
    statesSeenAndUnseen.cache()
    logging.info(f"States seen and unseen size: {statesSeenAndUnseen.count()}\n")
    logging.info(f"States seen and unseen: {statesSeenAndUnseen.collect()}\n")
    #print(f"This is the type of statesSeenAndUnseen: {type(statesSeenAndUnseen)}\n")

    # PLAN: calculate the false positives and true negatives, and then the False Positive Rate
    previouslyUnseen =statesSeenAndUnseen.filter(lambda x:x[0] not in previousStates)
    previouslyUnseen.cache()
    distinctPreviouslyUnseen=previouslyUnseen.distinct()
    logging.info(f"Distinct previously unseen states: {distinctPreviouslyUnseen.collect()} \n")
    logging.info(f"Distinct previously unseen size: {distinctPreviouslyUnseen.count()}\n")
    falsePositives = previouslyUnseen.filter(lambda x: x[1] == SEEN)
    trueNegatives = previouslyUnseen.filter(lambda x: x[1] == UNSEEN)
    #print(f"This is the type of falsePositives: {type(falsePositives)}\n")
    numFalsePositives = falsePositives.count()
    numTrueNegatives = trueNegatives.count()
    if numTrueNegatives+numFalsePositives==0:
        FPR=0.0
    else:
        FPR=numFalsePositives/(numFalsePositives+numTrueNegatives)
    logging.info(f"False pos at {time} is {numFalsePositives}\n"
        f"True neg at {time} is {numTrueNegatives}\n"
        f"FPR at {time} is {FPR}\n")

    # PLAN: update the Bloom filter and previous states
    distinctStates=states.distinct()
    partialFilters = distinctStates.map(lambda x: createPartialFilter(x, hashFunctions))
    newFilter = partialFilters.reduce(lambda accum, partial: accum | partial)
    globalBloomArray = modifyGlobalBloomFilter(newFilter)
    for stateDefinitelySeen in distinctStates.collect():
        previousStates.add(stateDefinitelySeen)
    logging.info(f"New filter at {time} is \n{bin(newFilter)}\n"
          f"Glo filter at {time} is \n{bin(globalBloomArray)}\n")
    if newFilter==globalBloomArray:
        logging.info("The new filter and the global filter are the same!")
    else:
        logging.info("The global filter is different from the new filter!")
    logging.info(f"Previous states at {time} is {previousStates}\n")

    # PLAN: write FPR to output file
    outputFile.write(f"{time},{FPR}\n")

    return None


if __name__=="__main__":

    logging.basicConfig(level=logging.INFO)
    logging.disable(level=logging.CRITICAL)

    #Below are the arbitrary constants
    seed=54
    hostName="localhost"
    batchDuration=10
    numBuckets=200
    numHashFunctions=3 #should be less than m/n, which is 200 targets/60 states=3.333

    # PLAN: collect commandline arguments
    portNum, outputFilename = sys.argv[1:]
    portNum=int(portNum)

    # PLAN: create the spark configuration and context objects
    conf = SparkConf().setAppName("Task1").setMaster("local[*]")
    sc = SparkContext(conf=conf)
    sc.setLogLevel("ERROR")

    # PLAN: open file
    outputFile=open(outputFilename,'w')
    outputFile.write("Time,FPR\n")

    # PLAN: create a global bit array that will be the backbone of the Bloom filter
    globalBloomArray=0

    # PLAN: create the hash functions that will be used in conjunction with the Bloom filter
    hashFunctions=createHashFunctions(numHashFunctions, numBuckets, seed)

    # PLAN: create previous values set that is essential for tracking the False Positive Rate
    previousStates=set()

    # PLAN: broadcast these variables
    gba=sc.broadcast(globalBloomArray)
    ps=sc.broadcast(previousStates)
    hf=sc.broadcast(hashFunctions)

    # PLAN: receive a batch of data
    ssc = StreamingContext(sc, batchDuration)
    lines = ssc.socketTextStream(hostName, portNum)

    # PLAN: process with Bloom filter
    lines.foreachRDD(lambda time, rdd:enactBloomFilter(time, rdd, outputFile))

    # PLAN: start and end the socket stream
    ssc.start()
    ssc.awaitTermination()

    # PLAN: close the output file
    outputFile.close()