'''
This file consists of task 2 of homework 4 for INF 553: Data Mining
It is focused on the implementation of the Girvan-Newman algorithm
'''

from pyspark import SparkContext, SparkConf
import sys
import time
import copy
import queue as q
from math import sqrt
import logging
#import itertools
#import random
#from functools import partial
#from operator import itemgetter

class girvanNode(object):
    def __init__(self, name, level, numPaths):
        self.name=name
        self.level=level
        self.numPaths=numPaths
        self.credit=1.0 #TODO: check if this is the right default value
        self.parents=list()
        self.children=list()
    def __str__(self):
        return f"Name={self.name}, Level={self.level}, Credit={self.credit}"
    def __repr__(self):
        return self.name

def repeatEdges(elem):
    yield (elem[0], elem[1])
    yield (elem[1], elem[0])

def removeEdgeFromGraph(edge, graph):
    node1, node2=edge
    graph[node1].remove(node2)
    graph[node2].remove(node1)
    return None

def sortByDescendingValueThenLexicographically(elem):
    return (-elem[1], elem[0][0])

def writeBetweenessFile(betweenessFilename, betweenessRDD):
    with open(betweenessFilename, 'w') as betweenFile:
        betweenessList=betweenessRDD.collect()
        for betweenessElem in betweenessList:
            betweenFile.write(f"{betweenessElem[0]}, {betweenessElem[1]}\n")

def writeCommunityFile(communityFilename, communities):
    with open(communityFilename, 'w') as communityFile:
        communitiesAsLists=[sorted(community) for community in communities]
        sortedByLenCommunities=sorted(communitiesAsLists, key=lambda x:(len(x), x[0]))
        for community in sortedByLenCommunities:
            communityStr=str(community)[1:-1]
            communityFile.write(communityStr+"\n")


def bfs(graph, initial):
    '''
    I took this simple BFS implementation from the link below:
    https://www.codespeedy.com/breadth-first-search-algorithm-in-python/
    :param graph: a dictionary whose keys are node names and whose values are lists of node names
    :param initial: name of the starting node
    :return: a list of the node names which are connected
    '''

    visited = []

    queue = [initial]

    while queue:

        node = queue.pop(0)
        if node not in visited:

            visited.append(node)
            neighbours = graph[node]

            for neighbour in neighbours:
                queue.append(neighbour)
    return visited

def findPartitions(graph):
    nodeReserve=set(graph.keys())
    partitions = list()
    # PLAN: take a node from the node reserve and run BFS
    while len(nodeReserve)!=0:
        newStartNode=nodeReserve.pop()
        newPartition=set(bfs(graph, newStartNode))
        partitions.append(newPartition)
        nodeReserve.difference_update(newPartition)
    # PLAN: remove these connected nodes from the node reserve
    # PLAN: repeat process until node reserve is empty
    return partitions

def modularity(graph, partitions, twiceNumEdges):
    modularityScore = 0
    # PLAN: calculate the number of edges * 2, as this will be used frequently throughout
    #twiceNumEdges=sum([len(value) for value in graph.values()])
    # PLAN: loop over the partitions
    for partition in partitions:
        partitionList=list(partition)
        # PLAN: double loop over the nodes of the partition
        for i in range(len(partitionList)):
            node1=partitionList[i]
            degreeOfNode1 = len(graph[node1])
            #node1Scores=list()
            for j in range(i+1, len(partitionList)):
                node2=partitionList[j]
                if node1!=node2:
                    degreeOfNode2 = len(graph[node2])
                    if node2 in graph[node1] and node1 in graph[node2]:
                        observed=1
                    else:
                        observed=0
                    # PLAN: perform fundamental calculation of observed edge minus expected edges
                    observedMinusExpected=observed-degreeOfNode1*degreeOfNode2/twiceNumEdges
                    modularityScore += observedMinusExpected
                    #node1Scores.append(observedMinusExpected)
                else:
                    pass #shouldn't calculate expected edges with the same node as itself
            #print(f"For node {node1}, the modularity fragments are {node1Scores}")
            #print(f"The sum for {node1} is {sum(node1Scores)}")

    # PLAN: divide by the number of edges*2 to normalize the modularity
    modularityScore/=twiceNumEdges
    return modularityScore

def betweeness(startNodeName, graph):
    '''
    Performs a modifed BFS to calculate the partial betweeness of an edge.
    BEST USED IN CONJUNCTION WITH FLATMAP
    :param startNodeName: the name of the root node of the BFS
    :param graph: a dictionary of the graph to traverse, with node names as keys
    and lists of their neighbors' names as values
    :yield: a tuple of the form ((nodename1, nodename2), partialBetweeness)
    '''

    # PLAN: Perform BFS to discern the level structure and parent/child relationships among the nodes
    # note: I used the implementation of BFS in the link below as a starting point
    # https://www.codespeedy.com/breadth-first-search-algorithm-in-python/
    explored=list()
    nameToLevel=dict()
    nameToNode=dict()
    levelToNodes=dict()
    queue=q.Queue()

    queue.put(startNodeName)
    nameToLevel[startNodeName]=0
    nameToNode[startNodeName]=girvanNode(startNodeName, level=0, numPaths=1)

    while not queue.empty():
        nodeName=queue.get()
        if nodeName not in explored:
            # preprocessing and bookkeeping with the current node
            explored.append(nodeName)
            neighbors = graph[nodeName]
            level = nameToLevel[nodeName]  # level is assigned by its parent before a node is explored, so this will work
            if nodeName!=startNodeName:
                node = girvanNode(nodeName, level=level, numPaths=0)
                nameToNode[nodeName] = node
            else:
                node = nameToNode[startNodeName]
            if level not in levelToNodes:
                levelToNodes[level] = list()
            levelToNodes[level].append(node)

            #now, check the neighbors
            for neighbor in neighbors:
                if neighbor not in nameToLevel:
                    # this neighbor is on a lower level, otherwise it would have already been noted
                    # i.e. the current node is a parent to the neighbor node
                    # I must do the parent/child assignment in the next conditional block, since a child node
                    # can have multiple parents and this would only catch one of them
                    nameToLevel[neighbor]=nameToLevel[nodeName]+1
                elif nameToLevel[neighbor]==nameToLevel[nodeName]-1:
                    # if a neighbor is a level above the current node,
                    # then that neighbor is a parent
                    parentNode=nameToNode[neighbor]
                    childNode=node
                    childNode.parents.append(parentNode)
                    childNode.numPaths+=parentNode.numPaths
                    parentNode.children.append(childNode)
                    #print(f"{nodeName} has parent {neighbor}")
                else:
                    # this neighbor is on the same level, and thus this edge is irrelevant
                    pass
                queue.put(neighbor)
            #print(f"After node {nodeName}, the nameToLevel dict is {nameToLevel}")
            #print(f"{node.name} has the following parents {node.parents}")

    # PLAN: Calculate the partial betweeness of each node and edge that was traversed during BFS
    levelsHighToLow=sorted(levelToNodes, reverse=True)
    edgesAndScores=list()
    for level in levelsHighToLow:
        for node in levelToNodes[level]:
            if len(node.children)==0:
                node.credit=1.0 #TODO: this might be unnecessary, check later
            # PLAN: add credit to the parent node and the edge that connects them
            if len(node.parents)!=0:
                parentsPathsSum=sum([parent.numPaths for parent in node.parents])
                for parent in node.parents:
                    #print(f"For BFS from {startNodeName}, {node} has parent {parent}")
                    proportionOfCredit=node.credit*parent.numPaths/parentsPathsSum
                    parent.credit+=proportionOfCredit
                    sortedEgde=tuple(sorted([node.name, parent.name]))
                    edgesAndScores.append((sortedEgde, proportionOfCredit))
    return edgesAndScores

def betweenessFlatMap(elem, graph):
    edgesAndScores=betweeness(elem, graph)
    # PLAN: Yield the edges and their partial betweeness scores
    # print(f"There were {len(edgesAndScores)} contributing edge(s) from BFS from node {startNodeName}")
    for edgeAndScore in edgesAndScores:
        # print(f"{startNodeName} gave the edge and score {edgeAndScore}")
        yield edgeAndScore

def betweenessSpark(uniqueNodesRDD, graph):
    return uniqueNodesRDD.flatMap(lambda x: betweenessFlatMap(x, graph)) \
                .reduceByKey(lambda a, n: a + n) \
                .mapValues(lambda x: x / 2) \
                .sortBy(lambda x: (-x[1], x[0][0]), ascending=True)

def betweenessNoSpark(uniqueNodesRDD, graph):
    uniqueNodes=uniqueNodesRDD.collect()
    totalEdgesAndScores=dict()
    for uniqueNode in uniqueNodes:
        edgesAndScores=betweeness(uniqueNode, graph)
        for edge, score in edgesAndScores:
            if edge not in totalEdgesAndScores:
                totalEdgesAndScores[edge]=score
            else:
                totalEdgesAndScores[edge]+=score
    halvedEdgesAndScores={k:v/2.0 for k,v in totalEdgesAndScores.items()}
    sortedEdgesAndScores=sorted(halvedEdgesAndScores.items(), key=sortByDescendingValueThenLexicographically)
    return sortedEdgesAndScores

def girvanNewman(uniqueNodesRDD, originalGraph):
    numUniqueNodes=uniqueNodesRDD.count()
    mutableGraph=copy.deepcopy(originalGraph)
    # PLAN: calculate the modularity of the initial setup
    oldPartitions=findPartitions(originalGraph)
    twiceNumEdgesInOriginalGraph = sum([len(value) for value in originalGraph.values()])
    oldModularity=modularity(originalGraph, oldPartitions, twiceNumEdgesInOriginalGraph)
    logging.info(f"Modularity of the initial graph is {oldModularity}")
    logging.info(f"Number of communities in initial graph is {len(oldPartitions)}")
    newModularity=oldModularity+1000 #this is a dummy value for the loop to start
    newPartitions=oldPartitions #dummy value for inner loop
    maxModularity=oldModularity
    maxPartitions=oldPartitions

    # PLAN: loop until modularity starts to decrease
    numEdgesExcised=0
    while oldModularity<newModularity+.0005: #TODO: replace with oldModularity<newModularity
        # TODO: place update statements for oldModularity and oldPartitions
        if (newModularity!=oldModularity+1000):
            oldModularity=newModularity
            oldPartitions=newPartitions
        # PLAN: inside loop, calculate betweeness
        while len(newPartitions)==len(oldPartitions):
            betweenStart=time.time()
            betweenessRDD = betweenessSpark(uniqueNodesRDD, mutableGraph)
            edgeToExcise = betweenessRDD.take(1)[0][0] #there's some cruft around this object, hence the slicing
            '''
            sortedEdgesAndScores=betweenessNoSpark(uniqueNodesRDD, mutableGraph)
            edgeToExcise=sortedEdgesAndScores[0][0]
            '''
            betweenEnd=time.time()
            #logging.info(f"Total time finding betweeness: {betweenEnd-betweenStart}")
            #logging.info(f"The edge to excise is {edgeToExcise}")

            if numEdgesExcised==0:
                writeBetweenessFile(betweenessFilename, betweenessRDD)

            # PLAN: excise the edge with the top betweeness
            removeEdgeFromGraph(edgeToExcise, mutableGraph)
            numEdgesExcised+=1

            # PLAN: find the new partitions that result from this excision of an edge
            newPartitions=findPartitions(mutableGraph)
        # PLAN: calculate the new modularity with these new partitions
        modularityStart=time.time()
        newModularity=modularity(originalGraph, newPartitions, twiceNumEdgesInOriginalGraph)
        modularityEnd=time.time()
        if newModularity>maxModularity:
            maxModularity=newModularity
            maxPartitions=newPartitions
        logging.info(f"The modularity after {numEdgesExcised} edges were excised is {newModularity}")
        logging.info(f"Number of communities in new graph is {len(newPartitions)}")
        #logging.info(f"Total time finding modularity: {modularityEnd-modularityStart}")
    communities=maxPartitions
    return communities

if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)
    logging.disable(level=logging.CRITICAL)

    # PLAN: collect commandline arguments that refer to input and output filenames
    inputFilename, betweenessFilename, communityFilename = sys.argv[1:]

    # PLAN: create the spark configuration and context objects, and read the input file
    conf = SparkConf().setAppName("Task2").setMaster("local[*]") #TODO: add local[*] to use all cores
    sc = SparkContext(conf=conf)
    sc.setLogLevel("WARN")

    # PLAN: start timer
    startTime=time.time()

    # PLAN: read the input file
    inputRDD=sc.textFile(inputFilename)\
                .map(lambda x:x.split(" "))

    # PLAN: build the graph
    redundantEdgesRDD=inputRDD.flatMap(repeatEdges)
    graphRDD=redundantEdgesRDD.groupByKey()
    uniqueNodesRDD=graphRDD.keys().distinct().persist()
    graph=graphRDD.mapValues(list).collectAsMap()
    #print(list(graph.items())[:5])
    #print(len(graph))
    '''
    betweenessRDD=uniqueNodesRDD.flatMap(lambda x:betweeness(x,graph))\
        .reduceByKey(lambda a,n: a+n)\
        .mapValues(lambda x:x/2)\
        .sortBy(lambda x:(-x[1], x[0][0]), ascending=True)
    writeBetweenessFile(betweenessFilename, betweenessRDD)
    '''

    communities=girvanNewman(uniqueNodesRDD, graph)
    writeCommunityFile(communityFilename, communities)



    # PLAN: end timer
    endTime=time.time()
    totalTime=endTime-startTime
    print(f"Duration: {totalTime}")