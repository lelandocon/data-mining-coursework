'''
This file consists of task 1 of homework 4 for INF 553: Data Mining
It is focused on the usage of Spark GraphFrames for community detection
'''


from pyspark import SparkContext, SparkConf, SQLContext
from pyspark.sql import SparkSession, Row, functions
import sys
import time
import logging
import os
from graphframes import *


def repeatEdges(elem):
    yield (elem[0], elem[1])
    yield (elem[1], elem[0])

if __name__=="__main__":

    os.environ["PYSPARK_SUBMIT_ARGS"] = ("--packages graphframes:graphframes:0.6.0-spark2.3-s_2.11")

    logging.basicConfig(level=logging.INFO)
    #logging.disable(level=logging.CRITICAL)

    #Below are the arbitrary constants
    d=1

    # PLAN: collect commandline arguments that refer to input and output filenames
    inputFilename, communityFilename = sys.argv[1:]

    # PLAN: create the spark configuration and context objects, and read the input file
    conf = SparkConf().setAppName("Task1").setMaster("local[*]") #TODO: add local[*] to use all cores
    sc = SparkContext(conf=conf)
    sc.setLogLevel("WARN")
    sqlCon= SQLContext(sc)
    #spark = SparkSession.builder.appName("Task1").master('local[*]').getOrCreate()

    # PLAN: start timer
    startTime=time.time()

    # PLAN: read the input file
    '''
    edgesDF=spark.read.text(inputFilename).toDF()
    print(edgesDF.take(5))
    '''
    vertexRow=Row('id')
    edgeRow=Row("src", "dst")
    inputRDD=sc.textFile(inputFilename).map(lambda x:x.split(" "))

    redundantEdgesRDD=inputRDD.flatMap(repeatEdges)
    uniqueNodesRDD=redundantEdgesRDD.keys().distinct().map(vertexRow)
    vertices = sqlCon.createDataFrame(uniqueNodesRDD)

    edgesRDD=inputRDD
    edges = sqlCon.createDataFrame(edgesRDD).toDF("src","dst")

    g=GraphFrame(vertices, edges)
    result=g.labelPropagation(5)
    '''
    result=sqlCon.createDataFrame([
        ('102', 1523),
        ('25', 79),
        ('92', 1523),
        ('87', 79),
        ('12', 1523),
        ('9', 8),
        ('72', 42)
    ], ['id', 'label'])
    '''
    #communityRDD=result.groupby('label').agg(functions.collect_list('id'))
    communityRDD=result.rdd.map(tuple)
    communities=communityRDD.map(lambda x:(x[1], x[0]))\
        .groupByKey()\
        .map(lambda x:sorted(x[1]))\
        .sortBy(lambda x: (len(x), x[0]))\
        .collect()
    with open(communityFilename, 'w') as communityFile:
        for community in communities:
            communityStr=str(community)[1:-1]
            communityFile.write(communityStr+"\n")