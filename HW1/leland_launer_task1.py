'''
This is task 1 from the first homework in INF 553: Data Mining
It concerns the exploration of the Yelp dataset
'''
from pyspark import SparkContext, StorageLevel
import json
import sys
import time
import logging
import os

if __name__=="__main__":

    logging.basicConfig(level=logging.INFO)
    #logging.disable(level=logging.CRITICAL)
    '''
    # PLAN: collect commandline arguments that refer to input and output filenames
    if len(sys.argv)!=3:
        print("Incorrect number of commandline arguments given. Please try again.")
        exit(-1)
        '''

    inputFilename=sys.argv[1]
    outputFilename=sys.argv[2]


    # PLAN: create the spark configuration and context objects, and read the input file
    sc = SparkContext(master="local", appName="Task1")
    sc.setLogLevel("WARN")
    inputRDD = sc.textFile(inputFilename)
    jsonRDD = inputRDD.map(json.loads).cache()


    # PLAN: task 1.a, find the total number of users
    totalUsers=jsonRDD.count()
    logging.info("This is the total number of users: %d" % totalUsers)


    # PLAN: task 1.c, find the number of distinct user names
    # PLAN: task 1.e, find the top 10 most popular usernames and the number of times they appear
    usernameSightingsRDD = jsonRDD.map(lambda x:[x['name'], 1]).reduceByKey(lambda count1,count2: count1+count2).cache()
    numDistinctNames = usernameSightingsRDD.count()
    logging.info("This is the number of distinct user names: %s" % numDistinctNames)
    top10Usernames = usernameSightingsRDD.sortBy(lambda x:x[1], False).take(10)
    logging.info("These are the top 10 most popular user names: %s" % top10Usernames)
    logging.info("This is the type of the 10 most popular user names: %s" % type(top10Usernames))
    usernameSightingsRDD.unpersist()

    # PLAN: task 1.d, find the number of users that joined Yelp in the year 2011
    numUsersJoinedIn2011 = jsonRDD.filter(lambda x: time.strptime(x['yelping_since'], '%Y-%m-%d %H:%M:%S').tm_year==2011).count()
    logging.info("This is the number of users who joined in 2011: %d" % numUsersJoinedIn2011)

    # PLAN: task 1.b, find the average number of written reviews of all users
    # PLAN: task 1.f, find the users ids of the 10 most profilic review writers
    # TODO: should I keep reduceByKey? In other words, do you expect there to be redundant user profiles in user.json?
    reviewsPerUserRDD = jsonRDD.map(lambda x: [x['user_id'], x['review_count']]).reduceByKey(
        lambda count1, count2: count1 + count2).cache()
    totalReviews = reviewsPerUserRDD.reduce(lambda accumulator, newItem: (accumulator[0], accumulator[1] + newItem[1]))[
        1]
    avgNumReviewsPerUser = totalReviews / float(totalUsers)
    logging.info("This is the total number of reviews: %s" % totalReviews)
    logging.info("This is the type of avgNumReviewsPerUser: %s" % type(avgNumReviewsPerUser))
    logging.info("This is the average number of reviews per user: %s" % avgNumReviewsPerUser)
    # TODO: do a distributed sort on the most prolific review writers
    top10Reviewers=reviewsPerUserRDD.sortBy(lambda x:x[1], False).take(10)
    logging.info("These are the top 10 most prolific writers: %s"%top10Reviewers)
    reviewsPerUserRDD.unpersist()

    # PLAN: create output JSON file
    # PLAN: create dictionary that will be used for the output JSON file
    outputDict = {'total_users':totalUsers,
                  'avg_reviews':avgNumReviewsPerUser,
                  'distinct_usernames': numDistinctNames,
                  'num_users': numUsersJoinedIn2011,
                  'top10_popular_names': top10Usernames,
                  'top10_most_reviews': top10Reviewers}

    with open(outputFilename, 'w') as outputFile:
        json.dump(outputDict, outputFile)

    sc.stop()

