'''
This is task 3 from the first homework in INF 553: Data Mining
It concerns exploring multiple RDDs in conjunction
'''

from pyspark import SparkContext
import json
import sys
import time
import logging
import os

if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)
    logging.disable(level=logging.CRITICAL)
    inputFilename1 = sys.argv[1]
    inputFilename2 = sys.argv[2]
    outputFilename1 = sys.argv[3]
    outputFilename2 = sys.argv[4]


    # PLAN: create the spark configuration and context objects, and read the input file
    sc = SparkContext(master="local", appName="Task3")
    sc.setLogLevel("WARN")
    reviewRDD = sc.textFile(inputFilename1).map(json.loads) #inputFilename1=reviewRDD is arbitrarily defined by the assignment prompt
    businessRDD = sc.textFile(inputFilename2).map(json.loads)

    # PLAN: part a, get the average stars per review for a given state
    # PLAN: map both the business and reviews down to pairRDDs with only the essential info
    IDandStars=reviewRDD.map(lambda x:[x['business_id'], x['stars']])
    IDandStates=businessRDD.map(lambda x:[x['business_id'], x['state']])

    # PLAN: join business data (where the state info is) with the reviews (where the stars info is) on the business ID
    IDStatesandStars=IDandStates.join(IDandStars)
    logging.info("This is what IDStatesandStars looks like: %s"%IDStatesandStars.take(3))

    # PLAN: map again so that you get rid of the business id and add another field for the counts of reviews in a state
    StatesAndStars=IDStatesandStars.map(lambda x: [x[1][0], (x[1][1], 1)]) #the last 1 in the list is for counts
    logging.info("This is what States and Stars looks like: %s" % StatesAndStars.take(3))

    # PLAN: reduce by key so that you get a sum of counts and a sum of review stars
    SumsOfStarsAndCounts=StatesAndStars.reduceByKey(lambda accum, x: [accum[0]+x[0], accum[1]+x[1]])
    logging.info("This is what Sums looks like: %s" % SumsOfStarsAndCounts.take(3))

    # PLAN: map again so that you divide the sum of review stars by the sum of counts
    averageStarsByState=SumsOfStarsAndCounts.map(lambda x: [x[0], x[1][0]/float(x[1][1])]).sortBy(lambda x:x[1], False)
    logging.info("Average stars by state: %s" % averageStarsByState.take(3))

    # PLAN: part b, compare collect vs take

    collectStart=time.time()
    allStates=averageStarsByState.collect()
    print(allStates[:5])
    collectEnd=time.time()
    collectTotal=collectEnd-collectStart

    takeStart=time.time()
    first5States=averageStarsByState.take(5)
    print(first5States)
    takeEnd=time.time()
    takeTotal=takeEnd-takeStart

    # PLAN: create output JSON file
    # PLAN: create dictionary that will be used for the output JSON file
    outputDict={'m1':collectTotal,
    'm2':takeTotal,
    'explanation':'''Method 1 using collect consumes more time than Method 2 using take. This is because
    collect requires the workers to communicate more profusely with the driver in order to give the contents
    of the entire RDD over to the driver. Take allows the workers to communicate only what is needed
    '''}

    with open(outputFilename2, 'w') as outputFile:
        json.dump(outputDict, outputFile)

    with open(outputFilename1, 'w') as outputFile:
        outputFile.write("state,stars\n")
        for stateStar in allStates:
            outputFile.write("{0[0]},{0[1]}\n".format(stateStar))