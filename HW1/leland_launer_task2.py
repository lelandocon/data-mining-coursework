'''
This is task 2 from the first homework in INF 553: Data Mining
It concerns customized partitioning of a specific RDD
'''

from pyspark import SparkContext
import json
import sys
import time
import logging
import os

def countFunc(givenIterator):
    '''
    i=0
    while True:
        i+=1
        yield i
        '''
    return len(list(givenIterator))

if __name__=="__main__":
    inputFilename = sys.argv[1]
    outputFilename = sys.argv[2]
    numPartitions = sys.argv[3]

    # PLAN: create the spark configuration and context objects, and read the input file
    sc = SparkContext(master="local", appName="Task2")
    sc.setLogLevel("WARN")
    inputRDD = sc.textFile(inputFilename)
    jsonRDD = inputRDD.map(json.loads).cache()

    # PLAN : complete task 1.f with the default number of partitions
    reviewersRDD = jsonRDD.map(lambda x: [x['user_id'], x['review_count']])
    numPartitionsDefault = reviewersRDD.getNumPartitions()
    numItemsDefault = reviewersRDD.glom().map(lambda x:len(x)).collect()
    startTimeDefault = time.time()
    top10Reviewers= reviewersRDD.sortBy(lambda x: x[1], False).take(10)
    endTimeDefault=time.time()
    totalTimeDefault=endTimeDefault-startTimeDefault



    # PLAN : complete task 1.f with the inputted number of partitions

    reviewersRDDPartition = jsonRDD.map(lambda x: [x['user_id'], x['review_count']]).partitionBy(numPartitions)
    numPartitionsCustom = reviewersRDDPartition.getNumPartitions()
    numItemsCustom=reviewersRDDPartition.glom().map(lambda x:len(x)).collect()
    startTimePartition = time.time()
    top10ReviewersPartition= reviewersRDDPartition.sortBy(lambda x: x[1], False).take(10)
    endTimePartition=time.time()
    totalTimePartition = endTimePartition-startTimePartition

    assert (top10Reviewers == top10ReviewersPartition)

    # PLAN: create output json
    outputDict = {
        'default':{
            "n_partition":numPartitionsDefault,
            "n_items":numItemsDefault,
            "exe_time": totalTimeDefault
        },
        'customized':{
            "n_partition":numPartitionsCustom,
            "n_items":numItemsCustom,
            "exe_time": totalTimePartition
        },
        'explanation':'''The customized partitioning provides faster results since the number of partitions
        chosen in the custom partitioning (8) is also the number of cores available in the local computer
        I am developing on (and also many other computers). Thus, each core can adjudicate to one partition,
        which lessens the amount of communication that needs to be done and thus makes everything faster'''
    }

    with open(outputFilename, 'w') as outputFile:
        json.dump(outputDict, outputFile)